package vn.edu.neu.toiec.usecases;

import vn.edu.neu.toiec.data.entities.Question;
import vn.edu.neu.toiec.presentation.model.QuestionRequest;
import vn.edu.neu.toiec.presentation.model.QuestionResponse;

import java.util.List;

public interface QuestionService {

    Question createQuestion(QuestionRequest questionRequest);

    List<QuestionResponse> getAllQuestions();

    Question update(QuestionResponse questionResponse);

    Question delete(QuestionResponse questionResponse);

}
