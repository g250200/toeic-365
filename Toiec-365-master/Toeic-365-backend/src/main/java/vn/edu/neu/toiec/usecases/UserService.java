package vn.edu.neu.toiec.usecases;

import vn.edu.neu.toiec.core.security.UserPrinciple;
import vn.edu.neu.toiec.data.entities.User;

import java.util.List;

public interface UserService {

    List<UserPrinciple> getAllUser();

    void delete(User user);

    UserPrinciple getCurrentUser();

    User update(User user);
}
