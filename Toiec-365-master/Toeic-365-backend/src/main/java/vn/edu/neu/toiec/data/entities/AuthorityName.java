package vn.edu.neu.toiec.data.entities;

public enum AuthorityName {
    ROLE_USER,
    ROLE_ADMIN
}
