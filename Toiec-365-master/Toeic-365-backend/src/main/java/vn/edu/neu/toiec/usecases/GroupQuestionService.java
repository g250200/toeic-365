package vn.edu.neu.toiec.usecases;

import vn.edu.neu.toiec.data.entities.GroupQuestion;
import vn.edu.neu.toiec.presentation.model.GroupQuestionRequest;
import vn.edu.neu.toiec.presentation.model.GroupQuestionResponse;

import java.util.List;

public interface GroupQuestionService {
    GroupQuestion createGroupQuestion(GroupQuestionRequest groupQuestionRequest);

    List<GroupQuestionResponse> getAllGroupQuestions();

    List<GroupQuestionResponse> getAllTitle(String numberPart);

    GroupQuestion update(GroupQuestionResponse groupQuestionResponse);

    GroupQuestion delete(GroupQuestionResponse groupQuestionResponse);

}
