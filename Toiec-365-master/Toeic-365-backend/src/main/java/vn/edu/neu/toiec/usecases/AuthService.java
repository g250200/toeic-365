package vn.edu.neu.toiec.usecases;

import vn.edu.neu.toiec.data.entities.User;
import vn.edu.neu.toiec.presentation.model.LoginRequest;
import vn.edu.neu.toiec.presentation.model.RegisterRequest;

import java.util.HashMap;

public interface AuthService {
    User register(RegisterRequest registerRequest);

    HashMap<String, Object> login(LoginRequest loginRequest);


}
